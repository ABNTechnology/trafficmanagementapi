<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace TrafficManagementAPI\Model;

use TrafficManagementAPI\Controller\VehicleAPIController;
use \TrafficManagementAPI\Utils\Utils;

/**
 * Model class representing one Customer item.
 */
final class VehicleModel {

   
    /** @var int */
    private $id;
    /** @var string */
    private $classname;
    /** @var string */
    private $hour;
    /** @var string */
    private $speed;
    /** @var string */
    private $count;
    
    private $transactiondatetime;
    /** @var string */
    private $class1averagespeed;
    /** @var string */
    private $class1count;
    
    private $class2averagespeed;
    /** @var string */
    private $class2count;
    
    private $speed0_10Count;
    private $speed10_20Count;
    private $speed20_30Count;
    private $speed30_40Count;
    private $speed40_50Count;
    private $speed50_60Count;
    private $speed60_70Count;
    private $speed70_80Count;
    private $speed80_90Count;
    private $speed90_100Count;
    private $speed100_110Count;
    private $speed110_120Count;


    public function __construct() {
        
    }

    
    //~ Getters & setters

    /**
     * @return int <i>null</i> if not persistent
     */
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        if ($this->id !== null
                && $this->id != $id) {
            throw new Exception('Cannot change identifier to ' . $id . ', already set to ' . $this->id);
        }
        if ($id === null) {
            $this->id = null;
        } else {
            $this->id = (int) $id;
        }
    }

    /**
     * @return int one of 1/2/3
     */
    public function getClass() {
        return $this->classname;
    }

    public function setClass($classname) {
        $this->classname = $classname;
    }
    
    public function getHour() {
        return $this->hour;
    }

    public function setHour($hour) {
       $this->hour = $hour;
    }
    public function getSpeed() {
        return $this->speed;
    }

    public function setSpeed($speed) {
       $this->speed = $speed;
    }
    public function getCount() {
        return $this->count;
    }

    public function setCount($count) {
       $this->count = $count;
    }
    
    public function gettransactiondatetime() {
        return $this->transactiondatetime;
    }
    public function settransactiondatetime($transactiondatetime) {
       $this->transactiondatetime = $transactiondatetime;
    }
    public function getclass1averagespeed() {
        return $this->class1averagespeed;
    }

    public function setclass1averagespeed($class1averagespeed) {
       $this->class1averagespeed = $class1averagespeed;
    }
    public function getclass1count() {
        return $this->class1count;
    }

    public function setclass1count($class1count) {
       $this->class1count = $class1count;
    }
    
    
    public function getclass2count() {
        return $this->class2count;
    }

    public function setclass2count($class2count) {
       $this->class2count = $class2count;
    }
    
    public function getclass2averagespeed() {
        return $this->class2averagespeed;
    }

    public function setclass2averagespeed($class2averagespeed) {
       $this->class2averagespeed = $class2averagespeed;
    }
    
    public function getspeed0_10Count() {
        return $this->speed0_10Count;
    }
    public function setspeed0_10Count($speed0_10Count) {
       $this->speed0_10Count = $speed0_10Count;
    }
    public function getspeed10_20Count() {
        return $this->speed10_20Count;
    }
    public function setspeed10_20Count($speed10_20Count) {
       $this->speed10_20Count = $speed10_20Count;
    }
    public function getspeed20_30Count() {
        return $this->speed20_30Count;
    }
    public function setspeed20_30Count($speed20_30Count) {
       $this->speed20_30Count = $speed20_30Count;
    }
    public function getspeed30_40Count() {
        return $this->speed30_40Count;
    }
    public function setspeed30_40Count($speed30_40Count) {
       $this->speed30_40Count = $speed30_40Count;
    }
    public function getspeed40_50Count() {
        return $this->speed40_50Count;
    }
    public function setspeed40_50Count($speed40_50Count) {
       $this->speed40_50Count = $speed40_50Count;
    }
    public function getspeed50_60Count() {
        return $this->speed50_60Count;
    }
    public function setspeed50_60Count($speed50_60Count) {
       $this->speed50_60Count = $speed50_60Count;
    }
    public function getspeed60_70Count() {
        return $this->speed60_70Count;
    }
    public function setspeed60_70Count($speed60_70Count) {
       $this->speed60_70Count = $speed60_70Count;
    }
    public function getspeed70_80Count() {
        return $this->speed70_80Count;
    }
    public function setspeed70_80Count($speed70_80Count) {
       $this->speed70_80Count = $speed70_80Count;
    }
    public function getspeed80_90Count() {
        return $this->speed80_90Count;
    }
    public function setspeed80_90Count($speed80_90Count) {
       $this->speed80_90Count = $speed80_90Count;
    }
    public function getspeed90_100Count() {
        return $this->speed90_100Count;
    }
    public function setspeed90_100Count($speed90_100Count) {
       $this->speed90_100Count = $speed90_100Count;
    }
    public function getspeed100_110Count() {
        return $this->speed100_110Count;
    }
    public function setspeed100_110Count($speed100_110Count) {
       $this->speed100_110Count = $speed100_110Count;
    }
    public function getspeed110_120Count() {
        return $this->speed110_120Count;
    }
    public function setspeed110_120Count($speed110_120Count) {
       $this->speed110_120Count = $speed110_120Count;
    }
    
    public function VehicleTransactionData($table,$Dtype1,$Dtype2,$Dtype3)
    {
        require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagementAPI/DAO/TrafficManagementDAO.php');
        $vehicleDAO=new \TrafficManagementDAO();
        
        return $vehicleDAO->GetVehicleDataUsingSP($table,$Dtype1,$Dtype2,$Dtype3);
    }
    public function VehicleHourlySpeedRangeData()
    {
        require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagementAPI/DAO/TrafficManagementDAO.php');
        $vehicleDAO=new \TrafficManagementDAO();
        
        return $vehicleDAO->GetVehicleHourlySpeedRangeData();
    }
}
