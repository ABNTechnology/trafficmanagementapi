<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use \TrafficManagementAPI\DAO\TrafficManagementDAO;
use \TrafficManagementAPI\Model\VehicleModel;
use \TrafficManagementAPI\Utils\Utils;

header("Content-Type:application/json");

 final class VehicleAPIController{
   function __construct() {
 }

 
 public function TrafficListing($table,$Dtype1,$Dtype2,$Dtype3){

    require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagementAPI/Model/VehicleModel.php');
    $vehicleModel=new VehicleModel();
    return $vehicleModel->VehicleTransactionData($table,$Dtype1,$Dtype2,$Dtype3);
 }
 
 public function VehicleHourlySpeedRangeData(){

    require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagementAPI/Model/VehicleModel.php');
    $vehicleModel=new VehicleModel();
    return $vehicleModel->VehicleHourlySpeedRangeData();
 }
 
 }
?>