<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use \TrafficManagementAPI\DAO\TrafficManagementDAO;
use \TrafficManagementAPI\Model\CustomerModel;
use \TrafficManagementAPI\Utils\Utils;

header("Content-Type:application/json");

 final class CustomerAPIController{
   function __construct() {
 }

 
 public function Listing(){

    require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagementAPI/Model/CustomerModel.php');
    $customerModel=new CustomerModel();
    return $customerModel->Customerlist();
 }

 public function CustomerDetail(){
    $id=$_GET['id'];
    $arrArgument=array('id'=>$id);
    $arrValue=loadModel('CustomerModel','ShowCustomerDetails',$arrArgument);
    loadView('CustomerView.php',$arrValue);
 }
}
?>