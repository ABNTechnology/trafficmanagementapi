<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use \TrafficManagementAPI\DAO\TrafficManagementDAO;
use \TrafficManagementAPI\Model\CustomerModel;
use \TrafficManagementAPI\Utils\Utils;

header("Content-Type:application/json");

if(!empty($_GET['Controller']))
{
	$controllername=$_GET['Controller'];
}
if(!empty($_GET['Function']))
{
	$functionname=$_GET['Function'];
}
//die($functionname.$controllername);
$fn = $_SERVER['DOCUMENT_ROOT']. '/TrafficManagementAPI/Controller/'.$controllername . '.php';
if(file_exists($fn)){
    require_once($fn);
    $controllerClass=$controllername;
    if(!method_exists($controllerClass,$functionname)){
       response(200,$controllername. $functionname . 'not found');
    }
    $obj=new $controllerClass;
    
    require_once($_SERVER['DOCUMENT_ROOT']. '/TrafficManagementAPI/Model/CustomerModel.php');
        
    $customerModel = new CustomerModel();
        
    if(!empty($_GET['Table']))
    {
        $table=$_GET['Table'];
    }
    
    if(!empty($_GET['Dtype1']))
    {
        $Dtype1=$_GET['Dtype1'];
    }
    if(!empty($_GET['Dtype2']))
    {
        $Dtype2=$_GET['Dtype2'];
    }
    if(!empty($_GET['Dtype3']))
    {
        $Dtype3=$_GET['Dtype3'];
    }
    if(empty($_GET['Table']))
    {
        $result=$obj->$functionname();
    }
    else
    {
        $result=$obj->$functionname($table,$Dtype1,$Dtype2,$Dtype3);
    }
    //$customerModel=$obj-> $functionname();
    if(empty($_GET['Table']))
    {
        response(200,$result);
    }
    else    
    {
        responseVehicle(200,$result,$Dtype1,$Dtype2,$Dtype3);
    }
}

function response($status,$result)
{
	header("HTTP/1.1 ".$status);
	
        $cn=0;
        //var_dump($result);
        $array = array();
        $response = array(); 
        foreach ($result as $data)
        {
            $elements = array('Class' =>$result[$cn]->getClass(),'Hour' =>(int)$result[$cn]->getHour(),'S10' =>(int)$result[$cn]->getspeed0_10Count(),'S20' =>(int)$result[$cn]->getspeed10_20Count(),'S30' =>(int)$result[$cn]->getspeed20_30Count(), 'S40' =>(int)$result[$cn]->getspeed30_40Count(),'S50' =>(int)$result[$cn]->getspeed40_50Count(),'S60' =>(int)$result[$cn]->getspeed50_60Count(),'S70' =>(int)$result[$cn]->getspeed60_70Count(),'S80' =>(int)$result[$cn]->getspeed70_80Count(),'S90' =>(int)$result[$cn]->getspeed80_90Count(),'S100' =>(int)$result[$cn]->getspeed90_100Count(),'S110' =>(int)$result[$cn]->getspeed100_110Count(),'S120' =>(int)$result[$cn]->getspeed110_120Count());
        
            array_push($response, $elements);
                $cn=$cn+1;
        }
        //    array_push($array, $result[$cn]->getClass());
        //    array_push($array, (int)$result[$cn]->getHour());
        //    array_push($array, (int)$result[$cn]->getspeed0_10Count());
        //    array_push($array, (int)$result[$cn]->getspeed10_20Count());
        //    array_push($array, (int)$result[$cn]->getspeed20_30Count());
        //    array_push($array, (int)$result[$cn]->getspeed30_40Count());
        //    array_push($array, (int)$result[$cn]->getspeed40_50Count());
        //    array_push($array, (int)$result[$cn]->getspeed50_60Count());
        //    array_push($array, (int)$result[$cn]->getspeed60_70Count());
        //    array_push($array, (int)$result[$cn]->getspeed70_80Count());
        //    array_push($array, (int)$result[$cn]->getspeed80_90Count());
        //    array_push($array, (int)$result[$cn]->getspeed90_100Count());
        //   array_push($array, (int)$result[$cn]->getspeed100_110Count());
        //    array_push($array, (int)$result[$cn]->getspeed110_120Count());
            
        //    $cn=$cn+1;
        //}
        
        //var_dump($array);
        //$response['status']=$status;
	//$response['data']=$array;
	//var_dump($response);
	$json_response = json_encode($response);
        echo $json_response;
}

function responseVehicle($status,$result,$Dtype1,$Dtype2,$Dtype3)
{
	header("HTTP/1.1 ".$status);
	
        $cn=0;
        //var_dump($result);
        //$array = array();
        
        //foreach ($result as $data)
        //{
        //    $vehicleData = array(
        //        array(
        //            "Class" => $result[$cn]->getClass(),
        //            "Speed" => $result[$cn]->getSpeed(),
        //            "Count" => $result[$cn]->getCount(),
        //        )
        //    );
        //    $cn=$cn+1;
        //}
        $dtypeMethod2='get'.$Dtype2;
        $dtypeMethod1='get'.$Dtype1;
        $dtypeMethod3='get'.$Dtype3;
        
        $response1 = array();    
        foreach ($result as $data) {
                //if($Dtype3=='Speed')
                //{
                
                
                if($Dtype2=="None")
                {
                    $elements = array($Dtype1 =>$result[$cn]->$dtypeMethod1(), $Dtype3 =>(int)$result[$cn]->$dtypeMethod3());
                }
                else
                {
                    $elements = array($Dtype2 => $result[$cn]->$dtypeMethod2(),$Dtype1 =>(int) $result[$cn]->$dtypeMethod1(), $Dtype3 =>(int)$result[$cn]->$dtypeMethod3());
                }
                //}
                //if($Dtype3=='Count')
                //{
                //    $elements = array($Dtype2 => $result[$cn]->$dtypeMethod2(),$Dtype1 =>(int) $result[$cn]->$dtypeMethod1(), $Dtype3 =>(int)$result[$cn]->$dtypeMethod3());
                //}
                array_push($response1, $elements);
                $cn=$cn+1;
            }
    
        //var_dump($array);
        //$response['status']=$status;
	$response=$response1;
	//var_dump($response);
	$json_response = json_encode($response);
        echo $json_response;
}