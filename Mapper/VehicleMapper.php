<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace TrafficManagementAPI\Mapper;

use TrafficManagementAPI\Model\VehicleModel;

final class VehicleMapper {

    public function __construct() {
    }
    
    public static function map(VehicleModel $vehicleModel, array $properties) {
        if (array_key_exists('id', $properties)) {
            $vehicleModel->setId($properties['id']);
        }
        if (array_key_exists('Class', $properties)) {
            $vehicleModel->setClass($properties['Class']);
        }
        if (array_key_exists('Hour', $properties)) {
            $vehicleModel->setHour($properties['Hour']);
        }
        
        if (array_key_exists('Speed', $properties)) {
            $vehicleModel->setSpeed($properties['Speed']);
        }
        if (array_key_exists('Count', $properties)) {
            $vehicleModel->setCount($properties['Count']);
        }
        if (array_key_exists('class1count', $properties)) {
            $vehicleModel->setclass1count($properties['class1count']);
        }
        if (array_key_exists('class1averagespeed', $properties)) {
            $vehicleModel->setclass1averagespeed($properties['class1averagespeed']);
        }
        if (array_key_exists('class2count', $properties)) {
            $vehicleModel->setclass2count($properties['class2count']);
        }
        if (array_key_exists('class2averagespeed', $properties)) {
            $vehicleModel->setclass2averagespeed($properties['class2averagespeed']);
        }
        if (array_key_exists('transactiondatetime', $properties)) {
            $vehicleModel->settransactiondatetime($properties['transactiondatetime']);
        }
        
        if (array_key_exists('Speed0_10_Count', $properties)) {
            $vehicleModel->setspeed0_10Count($properties['Speed0_10_Count']);
        }
        if (array_key_exists('Speed10_20_Count', $properties)) {
            $vehicleModel->setspeed10_20Count($properties['Speed10_20_Count']);
        }
        if (array_key_exists('Speed20_30_Count', $properties)) {
            $vehicleModel->setspeed20_30Count($properties['Speed20_30_Count']);
        }
        if (array_key_exists('Speed30_40_Count', $properties)) {
            $vehicleModel->setspeed30_40Count($properties['Speed30_40_Count']);
        }
        if (array_key_exists('Speed40_50_Count', $properties)) {
            $vehicleModel->setspeed40_50Count($properties['Speed40_50_Count']);
        }
        if (array_key_exists('Speed50_60_Count', $properties)) {
            $vehicleModel->setspeed50_60Count($properties['Speed50_60_Count']);
        }
        if (array_key_exists('Speed60_70_Count', $properties)) {
            $vehicleModel->setspeed60_70Count($properties['Speed60_70_Count']);
        }
        if (array_key_exists('Speed70_80_Count', $properties)) {
            $vehicleModel->setspeed70_80Count($properties['Speed70_80_Count']);
        }
        if (array_key_exists('Speed80_90_Count', $properties)) {
            $vehicleModel->setspeed80_90Count($properties['Speed80_90_Count']);
        }
        if (array_key_exists('Speed90_100_Count', $properties)) {
            $vehicleModel->setspeed90_100Count($properties['Speed90_100_Count']);
        }
        if (array_key_exists('Speed100_110_Count', $properties)) {
            $vehicleModel->setspeed100_110Count($properties['Speed100_110_Count']);
        }
        if (array_key_exists('Speed110_120_Count', $properties)) {
            $vehicleModel->setspeed110_120Count($properties['Speed110_120_Count']);
        }
        //return $VehicleModel;
    }

    private static function createDateTime($input) {
        return DateTime::createFromFormat('Y-n-j H:i:s', $input);
    }

}
